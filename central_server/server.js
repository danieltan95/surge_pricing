const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const promise = require('bluebird')
const http = require('http')
const serveStatic = require('serve-static')
const path = require('path')
const compression = require('compression')

app
  .use(bodyParser.json())
  .use(compression())
  .use(function(req, res, next) {
    var origin = req.headers.origin
    if (origin && origin.includes('localhost')) {
      res.setHeader('Access-Control-Allow-Origin', origin);
      res.header("Access-Control-Allow-Credentials", "true")
      res.header("Access-Control-Allow-Headers", "Origin,Content-Type, Authorization, x-id, Content-Length, X-Requested-With")
      res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS")
    }
    next()
  })

// We expose an REST API gateway for this, so that we can easily add authentication features in the future.

app
  .use('/', serveStatic(path.join(__dirname, 'static')))
  .get('/ping', (req, res) => res.send('pong'))
  .post('/echo', (req, res) => res.send(req.body))
  .use('/supply', require('./supplyRoutes'))
  .use('/demand', require('./demandRoutes'))
  .use('/traffic', require('./trafficRoutes'))
  .use('/weather', require('./weatherRoutes'))
  .use('/calc', require('./calcRoutes'))

http.createServer(app).listen(3000, '0.0.0.0', ()=>console.log(`Server started.`))
