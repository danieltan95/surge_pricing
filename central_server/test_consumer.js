const kafka = require('kafka-node')
const client = new kafka.Client('localhost:2181')
const Consumer = kafka.HighLevelConsumer
const options = { autoCommit: false, fetchMaxWaitMs: 1000, fetchMaxBytes: 1024 * 1024 }
const consumer = new Consumer(client, [{topic:'test'}], options)

consumer.on('message', (message) => {
  console.log(message)
})

consumer.on('error', (err) => {
  console.log('error', err)
})