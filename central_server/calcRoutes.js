const express = require('express')
const rp = require('request-promise')
const Promise = require('bluebird')
const _ = require('lodash')
const cache = require('express-redis-cache')()

const config = require('./config.json')

const router = express()

// use redis to buffer incoming location reports from multiple clients.
// we dont use kafka as the current architecture is a single client to single server flow, with 
// elasticsearch as the main point of truth for data.


const aggregation_query = (gte, lte) => ({
  'size': 0,
  'query': {
    'range': {
      'time': {
        'gte': gte || 'now-10m/m',
        'lt' : lte,
        'format': 'epoch_second'
      }
    }
  },
  'aggregations': {
    'location_grid': {
      'geohash_grid': {
        'field': 'location',
        'precision': 6
      }
    }
  }
})

const batch_query = (gte, lte, after) => ({
  'sort' : [
     { 'time' : 'desc'}
  ],
  'search_after': after ? [after] : undefined,
  'query': {
    'range': {
      'time': {
        'gte': gte,
        'lt' : lte,
        'format': 'epoch_second'
      }
    }
  }
})

const latest_query = {
  'query': {
    'match_all': {}
  },
  'size': 1,
  'sort': [
    {
      'time': {
        'order': 'desc'
      }
    }
  ]
}

router
  .get('/latestWeather', (req, res)=>{
    rp({
      method:'GET',
      uri:`${config.elasticsearch_host}/weather/doc/_search`,
      json:true,
      body: latest_query
    })
    .then(data=>data.hits.hits && data.hits.hits[0]._source)
    .then(data=>res.send(data))
  })
  .get('/trafficCongestion', 
    //cache.route({ expire: 60*10  }), //ten minutes expiration
    (req, res)=>{
      rp({
        method:'GET',
        uri:`${config.elasticsearch_host}/traffic/doc/_search`,
        json:true,
        body: aggregation_query(req.body.fromDateTime, req.body.toDateTime)
      })
      .then(data=>_.transform(data.aggregations.location_grid.buckets, (r, e)=>{r[e.key]=e.doc_count}, {}))
      .then(data=>res.send(data))
    })
  .get('/supplyDemandRatio', 
    //cache.route({ expire: 60*10  }), //ten minutes expiration
    (req, res)=>{
      Promise.all([
        rp({
          method:'GET',
          uri:`${config.elasticsearch_host}/supply/doc/_search`,
          json:true,
          body: aggregation_query(req.body.fromDateTime, req.body.toDateTime)
        }),
        rp({
          method:'GET',
          uri:`${config.elasticsearch_host}/demand/doc/_search`,
          json:true,
          body: aggregation_query(req.body.fromDateTime, req.body.toDateTime)
        }),
      ])
      .then(data=>data.map(body=>_.transform(body.aggregations.location_grid.buckets, (r, e)=>{r[e.key]=e.doc_count}, {})))
      .then(data=>{
        const supply = data[0]
        const demand = data[1]
        const supplyDemandRatio = _(supply)
                                    .map((count, geohash)=>{
                                      console.log(count, geohash)
                                      if(demand[geohash]){
                                        return [geohash, count/demand[geohash]]
                                      }
                                    })
                                    .filter(e=>!!e)
                                    .transform((r, e)=>{r[e[0]]=e[1]}, {})
                                    .value()
        res.send(supplyDemandRatio)
      })
    })
  .post('/batchView', (req, res)=>{
    //THis can be either supply, demand or traffic
    Promise.map(req.body.context, (index, i)=>rp({
      method:'GET',
      uri:`${config.elasticsearch_host}/${index}/doc/_search`,
      json:true,
      body: batch_query(req.body.fromDateTime, req.body.toDateTime, req.body.searchAfter&&req.body.searchAfter[i])
    }))
    .then(data=>{
      body = data.map(body=>body.hits.hits.map(hit=>{
        const body = hit._source
        return [body.driver_id, body.incoming_requests, body.travel_time_mins, body.trip_distance_km, body.location, body.time].filter(e=>!!e)
      }))
      searchAfter = data.map(body=>body.hits.hits.length && _.last(body.hits.hits).sort)
      res.send({
        body:body, 
        searchAfter: searchAfter
      })
    })
  })
module.exports = router