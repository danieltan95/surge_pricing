const queue = require('kue').createQueue()
const Promise = require('bluebird')

const createQueueJobPromise = (action, data) => {
  return new Promise((resolve, reject) => {
    queue
      .create(action, data)
      .removeOnComplete(true)
      .save()
      .on('progress', (progress, data) => resolve(data))
      .on('failed', (err)=>reject(err))
  })
}

module.exports = {
  createQueueJobPromise,
}