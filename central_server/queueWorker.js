const queue = require('kue').createQueue()
const rp = require('request-promise')

const config = require('./config.json')

queue.process('db:upload:driver:location', 10, (job, done) => {
  console.log(job.data)
  return rp({
    method:'POST',
    uri:`${config.elasticsearch_host}/supply/doc`,
    json:true,
    body:job.data
  })
  .then(body=>{
    job.progress(1, null, body)
    done()
  })
  .catch(err=>{
    job.failed().error(err)
    done(err)
  })
})

queue.process('db:upload:demand:location', 10, (job, done) => {
  return rp({
    method:'POST',
    uri:`${config.elasticsearch_host}/demand/doc`,
    json:true,
    body:job.data
  })
  .then(body=>{
    job.progress(1, null, body)
    done()
  })
  .catch(err=>{
    job.failed().error(err)
    done(err)
  })
})

queue.process('db:upload:traffic:location', 10, (job, done) => {
  return rp({
    method:'POST',
    uri:`${config.elasticsearch_host}/traffic/doc`,
    json:true,
    body:job.data
  })
  .then(body=>{
    job.progress(1, null, body)
    done()
  })
  .catch(err=>{
    job.failed().error(err)
    done(err)
  })
})


queue.process('db:upload:weather:current', 10, (job, done) => {
  return rp({
    method:'POST',
    uri:`${config.elasticsearch_host}/weather/doc`,
    json:true,
    body:job.data
  })
  .then(body=>{
    job.progress(1, null, body)
    done()
  })
  .catch(err=>{
    job.failed().error(err)
    done(err)
  })
})

console.log('queue started')
