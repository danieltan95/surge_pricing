import React, { Component } from 'react'
import * as shortid from 'shortid'

import './App.css'
import 'bulma/css/bulma.css'

import RealTimeGraph from './components/RealTimeGraph'
import BatchViewer from './components/BatchViewer'

export default class App extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      active_context:1,
      active_mode:0,
    }
  }
  render() {
    let mode_container = this.state.active_mode===0 ? 
                         <RealTimeGraph active_context={this.state.active_context}/> :
                         <BatchViewer active_context={this.state.active_context}/>
    return (
      <div className='App'>
        <div className='level' style={{marginBottom:'0em'}}>
          <div className='level-left'>
            {
             ['Supply/Demand Ratio', 'Traffic Congestion'].map((context, i)=>{
              let classes = 'button is-info is-margined-left'
              if(i!==this.state.active_context){
                classes+=' is-outlined'
              }
              return <a className={classes} key={shortid.generate()}onClick={()=>this.setState({active_context:i})}>{context}</a>
             })
            }
          </div>
          <div className='level-right'>
            {
             ['Real Time', 'Batch'].map((context, i)=>{
              let classes = 'button is-link is-margined-right'
              if(i!==this.state.active_mode){
                classes+=' is-outlined'
              }
              return <a className={classes} key={shortid.generate()}onClick={()=>this.setState({active_mode:i})}>{context}</a>
             })
            }
          </div>
        </div>
        <div>
          {mode_container}
        </div>
      </div>
    )
  }
}