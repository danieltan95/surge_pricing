import React, { Component } from 'react'
import moment from 'moment'
import TimePicker from 'rc-time-picker'
import DayPickerInput from 'react-day-picker/DayPickerInput'
import * as shortid from 'shortid'

import 'react-day-picker/lib/style.css'
import 'rc-time-picker/assets/index.css'

import {
  postJson
} from './UtilFunctions'

class BatchViewer extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      fromDateTime:undefined,
      toDateTime:moment(),
      tableData:[],
      searchAfterArr:[],
      searchAfter:undefined
    }
  }

  componentWillReceiveProps(nextProps) {
    // We clear the table data when the active_context is changed
    if (this.props.active_context !== nextProps.active_context) {
      this.setState({tableData: []});
    }
  }

  onPickFromDate(value){
    value=moment(value)
    let fromDateTime = this.state.fromDateTime || moment()
    fromDateTime.year(value.year())
    fromDateTime.month(value.month())
    fromDateTime.date(value.date())
    this.setState({
      fromDateTime : fromDateTime
    })
  }

  onPickFromTime(value){
    value = moment(value, 'HH:mm')
    let fromDateTime = this.state.fromDateTime || moment()
    fromDateTime.hour(value.hour())
    fromDateTime.minute(value.minute())
    this.setState({
      fromDateTime : fromDateTime
    })
  }

  onPickToTime(value){
    value = moment(value, 'HH:mm')
    let toDateTime = this.state.toDateTime || moment()
    toDateTime.hour(value.hour())
    toDateTime.minute(value.minute())
    this.setState({
      toDateTime : toDateTime
    })
  }

  onPickToDate(value){
    value=moment(value)
    let toDateTime = this.state.toDateTime || moment()
    toDateTime.year(value.year())
    toDateTime.month(value.month())
    toDateTime.date(value.date())
    this.setState({
      toDateTime : toDateTime
    })
  }

  submitQuery(){
    const req_body = {
      context:[['supply', 'demand'], ['traffic']][this.props.active_context]
    }
    if(this.state.toDateTime) req_body.toDateTime = moment(this.state.toDateTime).unix()
    if(this.state.fromDateTime) req_body.fromDateTime = moment(this.state.fromDateTime).unix()
    postJson('/calc/batchView', req_body)
    .then(data=>{
      this.setState({
        pageSize:data.body && data.body[0].length,
        tableData:data.body,
        searchAfterArr:data.searchAfter
      })
    })
    .catch(err=>console.log(err))
  }

  getNextPage(context){
    const req_body = {
      context:[['supply', 'demand'], ['traffic']][this.props.active_context],
      searchAfter:[]
    }
    console.log(this.state.searchAfterArr)
    req_body.searchAfter[context]=this.state.searchAfterArr[context][0]
    if(this.state.toDateTime) req_body.toDateTime = moment(this.state.toDateTime).unix()
    if(this.state.fromDateTime) req_body.fromDateTime = moment(this.state.fromDateTime).unix()
    postJson('/calc/batchView', req_body)
    .then(data=>{
      if(data.body){
        this.setState({
          tableData:data.body,
          searchAfterArr:data.searchAfter
        })
      }
    })
    .catch(err=>console.log(err))
  }

  generateTable(headers, body, context){
    return (
      <div key={shortid.generate()} className="column is-centered">
        <table 
          className="table " 
          style={{margin:'auto', display:'table', 'marginTop': '50px'}}
          key={shortid.generate()}
        >
          <thead>
            <tr>
              {headers.map(header=><th key={shortid.generate()} >{header}</th>)}
            </tr>
          </thead>
          <tfoot>
            <tr>
              {headers.map(header=><th key={shortid.generate()} >{header}</th>)}
            </tr>
          </tfoot>
          <tbody>
            {
              body.map(data=>{
                data[data.length-1] = moment.unix(data[data.length-1]).format('YYYY-MM-DD HH:mm')
                return <tr key={shortid.generate()} >
                {
                  data.map(col=><td key={shortid.generate()}>{col}</td>)
                }
                </tr>
                }
              )
            }
          </tbody>
        </table>
        <div>
          <a className="pagination-next" onClick={()=>this.getNextPage(context)}>Next page</a>
        </div>
      </div>
    )
  }

  render() {
    const head_data = [
      [
        ['Driver Id', 'Location', 'Time'],['Incoming Requests', 'Location', 'Time'],
      ],
      [
        ['Travel Time (mins)', 'Trip Distance (km)', 'Location', 'Time']
      ]
    ]
    return (
      <div>
        <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
          From:
          <TimePicker
            showSecond={false}
            onChange={this.onPickFromTime.bind(this)}
          />
          <DayPickerInput onDayChange={this.onPickFromDate.bind(this)}/>
          <span style={{paddingLeft:'10px'}}></span>
          To:
          <TimePicker
            showSecond={false}
            defaultValue={moment()}
            onChange={this.onPickToTime.bind(this)}
          />
          <DayPickerInput onDayChange={this.onPickToDate.bind(this)}/>
          <span style={{paddingLeft:'10px'}}></span>
          <a className="button" onClick={this.submitQuery.bind(this)}>Go</a>
        </div>
        <div className="columns is-centered">
          {this.state.tableData.map((data,i)=>this.generateTable(head_data[this.props.active_context][i], data, i))}
        </div>
      </div>
    )
  }
}


export default BatchViewer