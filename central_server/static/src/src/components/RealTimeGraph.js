import React, { Component } from 'react'
import * as d3 from 'd3'
import * as d3ScaleChromatic from 'd3-scale-chromatic'
import * as Geohash from 'latlon-geohash'
import * as _ from 'lodash'
import * as topojson from 'topojson'
import { legendColor } from 'd3-svg-legend'
import * as moment from 'moment'

import {getJson} from './UtilFunctions'
import * as us from './nyc.json'

export default class RealTimeGraph extends Component {
  constructor(props) {
    super(props)
  
    this.state = {
      weatherInfo:{}
    }
  }

  componentWillReceiveProps(nextProps) {
    // We clear the table data when the active_context is changed
    if (this.props.active_context !== nextProps.active_context) {
      d3.selectAll('circle').remove()
    }
  }

  componentDidMount() {
    // Draw new york map
    const width = 1000
    const height = 700
    const svg = d3.select('#RealTimeGraph-SVG').append('svg')
                  .attr('width', width)
                  .attr('height', height)
                  .attr('transform', `translate(-${width/2}*(0.8-1), -${height/2}*(0.8-1))`)
                  

    const linear = d3.scaleSequential(d3ScaleChromatic.interpolateRdYlGn).domain([1,0])
    console.log(linear)

    /*
    const projection = d3.geoAlbersUsa()
                        .scale(6193)
                        .translate([width / 2, height / 2])
    */
    const projection = d3.geoMercator().center([-75.819, 42.795]).scale(6193).translate([width / 2, height / 2]).precision(0)
    svg.append("path")
      .datum(topojson.mesh(us))
      .attr("d", d3.geoPath());
    /*
    svg.append('g')
      .attr('class', 'states')
      .selectAll('path')
      .data(topojson.feature(us, us.objects.states).features)
      .enter().append('path')
      .attr('d', path)
    svg.append('path')
        .attr('class', 'state-borders')
        .attr('d', path(topojson.mesh(us, us.objects.states, (a, b) => a !== b )))
    */

    // draw color legend
    svg.append('g')
      .attr('class', 'legendLinear')
      .attr('transform', `translate(${width-100},20)`)

    const legendLinear = legendColor()
      .ascending(true)
      .shapeWidth(30)
      .cells(10)
      .orient('vertical')
      .scale(linear)

    svg.select('.legendLinear')
      .call(legendLinear)

    // Refresh sources and draw points
    const decodeGeoHash = (geohash)=>{
      const decoded = Geohash.decode(geohash)
      return [decoded.lon, decoded.lat]
    }
    const refreshPoints = ()=>{
      console.log('loading...')
      svg.append('text')
        .text('Loading...')
        .attr('class','loading')
        .attr('transform', `translate(${width-100},10)`)
      const url_path = ['supplyDemandRatio', 'trafficCongestion'][this.props.active_context] 
      getJson(`/calc/${url_path}`)
        .then(data=>{
          console.log('loading complete')
          data = _.toPairs(data)
          data = data.filter(e=>!!projection(decodeGeoHash(e[0])))
          const points = data.map(e=>decodeGeoHash(e[0]))
          const traffic_congestion = data.map(e=>e[1])
          const max_traffic_congestion = _.max(traffic_congestion)
          const scaled_traffic_congestion = traffic_congestion.map(point => (point/(max_traffic_congestion)))
          data = points.map((d,i)=>[d, scaled_traffic_congestion[i]])
          console.log(points)
          data = _.sortBy(data, e=>e[1])
          svg.selectAll('circle')
            .data(data).enter()
            .append('circle')
            .attr('cx', d => projection(d[0])[0])
            .attr('cy', d => projection(d[0])[1])
            .attr('r', d => `${(10*d[1])+5}px`)
            .attr('fill-opacity', d=>(0.7+d[1]*0.1))
            .attr('fill', d => d3ScaleChromatic.interpolateRdYlGn(Math.abs(d[1]-1)))  
            .append('title').text(d=>`lat: ${d[0][1]} long: ${d[0][0]}`)
          svg.select('.loading').remove()
      })
    }
    refreshPoints()
    setInterval(refreshPoints,5000)

    const refreshWeather = ()=>{
      getJson(`/calc/latestWeather`)
        .then(data=>{
          this.setState({
            weatherInfo:data
          })
        })
    }
    refreshWeather()
    setInterval(refreshWeather,5000)
  }

  render() {
    const weatherInfo = this.state.weatherInfo
    const loadingIfUndefined = (data)=>data===undefined ? 'Loading...' : data
    const weatherInfoComponent = (
      <table class="table">
        <tbody>
          <tr>
            <th className="has-text-weight-bold">Temperature (Degrees)</th>
            <th className="has-text-weight-normal">{loadingIfUndefined(weatherInfo.temperature_degrees)}</th>
          </tr>
          <tr>
            <th className="has-text-weight-bold">Total Precipitation</th>
            <th className="has-text-weight-normal">{loadingIfUndefined(weatherInfo.total_precipitation)}</th>
          </tr>
          <tr>
            <th className="has-text-weight-bold">Wind Speed (km/h)</th>
            <th className="has-text-weight-normal">{loadingIfUndefined(weatherInfo.wind_speed)}</th>
          </tr>
          <tr>
            <th className="has-text-weight-bold">Wind Direction</th>
            <th className="has-text-weight-normal">{loadingIfUndefined(weatherInfo.wind_direction)}</th>
          </tr>
          <tr>
            <th className="has-text-weight-bold">Last Updated:</th>
            <th className="has-text-weight-normal">{loadingIfUndefined(weatherInfo.time && moment.unix(weatherInfo.time).format('YYYY-MM-DD HH:mm'))}</th>
          </tr>
        </tbody>
      </table>
    )
    return (
      <div>
        <style>{`
          path {
            fill: none;
            stroke: #000;
            stroke-width: .5px;
            stroke-linejoin: round;
            stroke-linecap: round;
          }
          .state-borders {
            fill: none;
            stroke: #fff;
            stroke-width: 0.5px;
            stroke-linejoin: round;
            stroke-linecap: round;
            pointer-events: none;
          }
          `}
        </style>
        <div className='columns'>
          <div className='column is-one-quarter' style={{marginTop:'50px'}}>
            <p className='is-size-5 className="has-text-weight-normal"'>Weather Condition</p>
            {weatherInfoComponent}
          </div>
          <div className='column' id='RealTimeGraph-SVG'></div>
        </div>
      </div>
    )
  }
}
