const hostname = `${window.location.protocol}//${window.location.hostname}:3000`

const getJson = (relativeurl) =>
                  fetch(`${hostname}${relativeurl}`)
                    .then(res => res.json())
                    .catch(err => {
                      console.log(err)
                      return null
                    })

const postJson = (relativeurl, body) =>
                  fetch(`${hostname}${relativeurl}`, {
                    headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                    method: 'POST',
                    body: JSON.stringify(body)
                  })
                  .then(res => res.json())
                  .catch(err => {
                    console.log(err)
                    return null
                  })

export {
  getJson,
  postJson,
}