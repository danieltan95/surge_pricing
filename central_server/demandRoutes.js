const express = require('express')
const { createQueueJobPromise } = require('./queueUtil')

const router = express()

// use redis to buffer incoming location reports from multiple clients.
// we dont use kafka as the current architecture is a single client to single server flow, with 
// elasticsearch as the main point of truth for data.

router
  .post('/reportLocation', (req, res)=>{
    if(!req.body.incoming_requests || !req.body.location || !req.body.time){
      return res.status(401).send({
        error:true,
        errorMsg:'MalformedUserData'
      })
    }
    return createQueueJobPromise('db:upload:demand:location', req.body)
            .then(data=>{
              console.log(data)
              if(!data) throw new Error('EmptyResult')
              return res.send({
                success:true
              })
            })
            .catch(err=>{
              return res.status(500).send({
                error:true,
                errorMsg:err
              })
            })
  })
module.exports = router