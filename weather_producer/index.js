const Promise = require('bluebird')
const rp = require('request-promise')
const moment = require('moment')

const exec = require('child_process').exec
const fs = Promise.promisifyAll(require("fs"))

const config = require('./config.json')

// This lookup table contains the information needed to convert a location name to latlon coordinates
const getLookUpTable = (file_name) => {
  return fs.readFileAsync(file_name, "utf8").then((contents) => {
    return contents.split('\n').map(line=>{
      line = line.split('|')
      return {
        location:line[0],
        lat:line[1],
        lon:line[2]
      }
    })
  })
}

//We simulate a stream of data using this client, which takes the last line of a large csv database and sends it off to our endpoint.
const generateData = (source_file_name) =>{
  const result_file_name = 'result'
  // Read the last line of the file and delete it.
  const command = `tail -1 ${source_file_name}; 
                   head -n -1 ${source_file_name} > "temp" ; 
                   mv "temp" ${source_file_name};`
  return new Promise((resolve, reject)=>{
    exec(command, (err, stdout, stderr)=>{
      if(err){
        reject(err)
      }else if(stderr){
        reject(stderr)
      }else if(stdout){
        resolve(stdout)
      }
    })
  })
}

//Talk to the kafka service about the current location of the driver
const publishLocationInfo = (weather_info) => {
  return rp({
    method: 'POST',
    //uri: 'https://httpbin.org/post',
    uri: 'http://localhost:3000/weather/reportCurrent',
    body: weather_info,
    json: true 
  })
  .then(data=>console.log(data))
  .catch(err=>console.log(err))
}

//Cross reference the location with time. Driver id uses a placeholder now.
const processData = (csv_line) =>{
  csv_line = csv_line.split(';') 
  return {
    temperature_degrees:+csv_line[5],
    total_precipitation:+csv_line[6],
    wind_speed:+csv_line[7],
    wind_direction:+csv_line[8],
    time: moment().unix()
  }
}

generateData('./nyc_weather.copy.csv').then(processData).then(publishLocationInfo)
setInterval(()=>{
  generateData('./nyc_weather.copy.csv').then(processData).then(publishLocationInfo)
}, 5000)