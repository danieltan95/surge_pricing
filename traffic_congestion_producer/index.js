const Promise = require('bluebird')
const rp = require('request-promise')
const moment = require('moment')
var Geohash = require('latlon-geohash')

const exec = require('child_process').exec
const fs = Promise.promisifyAll(require("fs"))

const config = require('./config.json')

// This lookup table contains the information needed to convert a location name to latlon coordinates
const getLookUpTable = (file_name) => {
  return fs.readFileAsync(file_name, "utf8").then((contents) => {
    return contents.split('\n').map(line=>{
      line = line.split('|')
      return {
        location:line[0],
        lat:line[1],
        lon:line[2]
      }
    })
  })
}

//We simulate a stream of data using this client, which takes the last line of a large csv database and sends it off to our endpoint.
const generateData = (source_file_name) =>{
  const result_file_name = 'result'
  // Read the last line of the file and delete it.
  const command = `tail -1 ${source_file_name}; 
                   head -n -1 ${source_file_name} > "temp" ; 
                   mv "temp" ${source_file_name};`
  return new Promise((resolve, reject)=>{
    exec(command, (err, stdout, stderr)=>{
      if(err){
        reject(err)
      }else if(stderr){
        reject(stderr)
      }else if(stdout){
        resolve(stdout)
      }
    })
  })
}

//Talk to the kafka service about the current location of the driver
const publishLocationInfo = (location_info) => {
  return rp({
    method: 'POST',
    //uri: 'https://httpbin.org/post',
    uri: 'http://localhost:3000/traffic/reportLocation',
    body: location_info,
    json: true 
  })
  .then(data=>console.log(data))
  .catch(err=>console.log(err))
}

//Cross reference the location with time. Driver id uses a placeholder now.
const processData = (csv_line) =>{
  csv_line = csv_line.split(',') 
  return getLookUpTable('../lookup_result.txt')
    .then(lookup_table=>{
      let location_id = csv_line[7]
      let location_data = lookup_table[location_id]
      let travel_time_mins = moment(csv_line[2]).diff(moment(csv_line[1]), 'minutes')
      return {
        travel_time_mins: +travel_time_mins,
        trip_distance_km: +csv_line[4],
        location:Geohash.encode(location_data.lat, location_data.lon, 6),
        time: moment().unix()
      }
    })
}

generateData('../source/source.copy.csv').then(processData).then(publishLocationInfo)
setInterval(()=>{
  generateData('../source/source.copy.csv').then(processData).then(publishLocationInfo)
}, 5000)